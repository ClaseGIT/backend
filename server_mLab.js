const express = require('express');
require ('dotenv').config();
const body_parser = require('body-parser');
const request_json = require('request-json');
const app = express();
const port = process.env.PORT || 3000;
const usersFile = require('./user.json');
const URL_BASE = '/techu/v2/';
const URL_mLab = 'https://api.mlab.com/api/1/databases/techu30db/collections/';
const apikey_mlab = 'apiKey=' + process.env.API_KEY_MLAB;

app.listen(port, function(){
  console.log('Node JS escuchando en el puerto ' + port);
});

app.use(body_parser.json());

//Operación GET con mLab(Collection)
app.get(URL_BASE + 'users',
  function(req, res){
  const http_client = request_json.createClient(URL_mLab);
  console.log('Cliente HTTP a mLab creado correctamente');
  let field_param = 'f={"_id":0}&';
  http_client.get('user?' + field_param + apikey_mlab,
  function (error, res_mlab, body) {
     console.log('Error: ' + error);
     console.log('Respuesta MLab: ' + JSON.stringify(res_mlab));
     console.log('Body: ' + body);
     var response = {};
     if(error) {
         response = {"msg" : "Error al recuperar users de mLab."}
         res.status(500);
     } else {
       if(body.length > 0) {
         response = body;
       } else {
         response = {"msg" : "Usuario no encontrado."};
         res.status(404);
       }
     }
     res.send(response);
   });
});

//Operacion GET users con ID con mLab
app.get(URL_BASE + 'users/:id',
  function (req, res) {
    var id = req.params.id;
    var queryString = 'q={"id_user":' + id + '}&';
    var queryStrField = 'f={"_id":0}&';
    var httpClient = request_json.createClient(URL_mLab);
    httpClient.get('user?' + queryString + queryStrField + apikey_mlab,
      function(err, respuestaMLab, body){
        console.log("respuestaMLab: " + JSON.stringify(respuestaMLab));
        var response = {};
        if(err) {
            response = {"msg" : "Error obteniendo usuario."}
            res.status(500);
        } else {
          if(body.length > 0) {
            response = body;
          } else {
            response = {"msg" : "Usuario no encontrado."}
            res.status(404);
          }
        }
        res.send(response);
      });
});

//Operacion GET accounts de users con ID con mLab
app.get(URL_BASE + 'users/:id/accounts',
  function (req, res) {
    console.log("request.params.id: " + req.params.id);
    var id = req.params.id;
    var queryString = 'q={"id_user":' + id + '}&';
    var queryStrField = 'f={"_id":0}&';
    var httpClient = request_json.createClient(URL_mLab);
    httpClient.get('user?' + queryString + queryStrField + apikey_mlab,
      function(err, res_mlab, body){
        console.log('Error: ' + err);
        console.log('Respuesta MLab: ${JSON.stringify(res_mlab)}');
        console.log('Body: ${JSON.stringify(body)}');
        var response = {};
        if(err) {
            response = {"msg" : "Error obteniendo usuario."}
            res.status(500);
        } else {
          if(body.length > 0) {
            response = body;
          } else {
            response = {"msg" : "Usuario no encontrado."}
            res.status(404);
          }
        }
        res.send(response);
      });
});

//Petición POST a users con mLab
app.post(URL_BASE + 'users',
    function(req, res){
      var httpClient = request_json.createClient(URL_mLab);
      console.log(req.body);
      httpClient.get('user?' + apikey_mlab,
      function(error, respuesta_mlab, body){
        newID = body.length + 1;
        console.log('newID: ' + newID);
        var newUser = {
        "id_user" : newID,
        "first_name" : req.body.first_name,
        "last_name" : req.body.last_name,
        "email" : req.body.email,
        "password" : req.body.password
      };
      httpClient.post(URL_mLab + 'user?' + apikey_mlab, newUser,
      function(error, respuesta_mlab, body){
        console.log(body);
        console.log(body.first_name);
        res.send(body);
      });
    });
});

//PUT users con parámetro 'id'
app.put(URL_BASE + 'users/:id',
function(req, res) {
  var id = req.params.id;
  var queryStringID = 'q={"id_user":' + id + '}&';
  var httpClient = request_json.createClient(URL_mLab);
  httpClient.get('user?'+ queryStringID + apikey_mlab,
    function(error, respuestaMLab, body) {
     var cambio = '{"$set":' + JSON.stringify(req.body) + '}';
     console.log(req.body);
     console.log(cambio);
     httpClient.put(URL_mLab +'user?' + queryStringID + apikey_mlab, JSON.parse(cambio),
      function(error, respuestaMLab, body) {
        console.log("body:"+ body);
        res.send(body);
      });
    });
});

//DELETE user with id
app.delete(URL_BASE + "users/:id",
  function(req, res){
    console.log("entra al DELETE");
    console.log("request.params.id: " + req.params.id);
    var id = req.params.id;
    var queryStringID = 'q={"id_user":' + id + '}&';
    console.log(URL_mLab + 'user?' + queryStringID + apikey_mlab);
    var httpClient = request_json.createClient(URL_mLab);
    httpClient.get('user?' +  queryStringID + apikey_mlab,
      function(error, respuestaMLab, body){
        var respuesta = body[0];
        console.log("body delete:"+ JSON.stringify(respuesta));
        httpClient.delete(URL_mLab + "user/" + respuesta._id.$oid +'?'+ apikey_mlab,
          function(error, respuestaMLab,body){
            res.send(body);
        });
      });
});

// DELETE con PUT de mLab
app.delete(URL_BASE + "users/:id",
  function(req, res){
    var id = Number.parseInt(req.params.id);
    var query = 'q={"id_user":' + id + '}&';
    // Interpolación de expresiones
    var query2 = `q={"id_user":${id} }&`;
    var query3 = "q=" + JSON.stringify({"id_user": id}) + '&';
    httpClient = request_json.createClient(URL_mLab);
    httpClient.put("user?" + query + apikey_mlab, [{}],
        function(err, resMLab, body) {
          var response = !err ? body : {
            "msg" : "Error borrando usuario."
          }
        res.send(response);
      }
    );
});

//Method POST login
app.post(URL_BASE + "login",
  function (req, res){
    console.log("POST /colapi/v3/login");
    let email = req.body.email;
    let pass = req.body.password;
    let queryString = 'q={"email":"' + email + '","password":"' + pass + '"}&';
    let limFilter = 'l=1&';
    let clienteMlab = request_json.createClient(URL_mLab);
    clienteMlab.get('user?'+ queryString + limFilter + apikey_mlab,
      function(error, respuestaMLab, body) {
        if(!error) {
          if (body.length == 1) { // Existe un usuario que cumple 'queryString'
            let login = '{"$set":{"logged":true}}';
            clienteMlab.put('user?q={"id_user": ' + body[0].id_user + '}&' + apikey_mlab, JSON.parse(login),
            //clienteMlab.put('user/' + body[0]._id.$oid + '?' + apikeyMLab, JSON.parse(login),
              function(errPut, resPut, bodyPut) {
                res.send({'msg':'Login correcto', 'user':body[0].email, 'userid':body[0].id_user, 'name':body[0].first_name});
                // If bodyPut.n == 1, put de mLab correcto
              });
          }
          else {
            res.status(404).send({"msg":"Usuario no válido."});
          }
        } else {
          res.status(500).send({"msg": "Error en petición a mLab."});
        }
    });
});

//Method POST logout
app.post(URL_BASE + "logout",
  function(req, res) {
    console.log("POST /colapi/v3/logout");
    var email = req.body.email;
    var queryString = 'q={"email":"' + email + '","logged":true}&';
    console.log(queryString);
    var clienteMlab = request_json.createClient(URL_mLab);
    clienteMlab.get('user?'+ queryString + apikey_mlab,
      function(error, respuestaMLab, body) {
        var respuesta = body[0]; // Asegurar único usuario
        if(!error) {
          if (respuesta != undefined) { // Existe un usuario que cumple 'queryString'
            let logout = '{"$unset":{"logged":true}}';
            clienteMlab.put('user?q={"id_user": ' + respuesta.id_user + '}&' + apikey_mlab, JSON.parse(logout),
            //clienteMlab.put('user/' + respuesta._id.$oid + '?' + apikeyMLab, JSON.parse(logout),
              function(errPut, resPut, bodyPut) {
                console.log("body logout: "+ JSON.stringify(respuesta));
                res.send({'msg':'Logout correcto', 'user':respuesta.email});
                // If bodyPut.n == 1, put de mLab correcto
              });
            } else {
                res.status(404).send({"msg":"Logout failed!"});
            }
        } else {
          res.status(500).send({"msg": "Error en petición a mLab."});
        }
    });
});
