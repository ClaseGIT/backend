const express = require('express');
require ('dotenv').config();
const body_parser = require('body-parser');
const app = express();
const port = process.env.PORT || 3000;
const URL_BASE = '/techu/v1/';
const usersFile = require('./user.json');

app.listen(port, function(){
  console.log('Node JS escuchando en el puerto ' + port);
});

app.use(body_parser.json());

//Operación GET (Collection)
app.get(URL_BASE + 'users',
  function(request, response){
  //response.status(200);
  response.status(200).send(usersFile);
});

//Peticion get a un unico usuario mediante ID (Instancia)
app.get(URL_BASE + 'users/:id',
  function(request, response){
  console.log(request.params.id);
  let pos = request.params.id - 1;
  let user = usersFile[pos];
  let status = (user == undefined) ? 204 : 200;
  response.status(status);
  response.send(user);
});

//GET con query string
app.get(URL_BASE + 'usersq',
function(request, response){
  console.log(request.query.id);
  console.log(request.query.country);
  response.send({"msg":"GET con query"});
});

//GET con total de usuarios
app.get(URL_BASE + 'userstot',
function(request, response){
  userstot = usersFile.length;
  response.status(200).send({"msg":"El total de usuarios es " + userstot});
});

//Petición POST a users
app.post(URL_BASE + 'users',
    function(request, response){
    console.log('POST a users');
    let tam = usersFile.length;
    let new_user = {
      "id_user":tam + 1,
      "first_name":request.body.first_name,
      "last_name":request.body.last_name,
      "email":request.body.email,
      "password":request.body.password
    }
    console.log(new_user);
    usersFile.push(new_user);
    response.status(201);
    response.send({"msg":"Usuario creado correctamente"});
});

//Petición PUT a users
app.put(URL_BASE + 'users/:id',
    function(request, response){
    console.log('PUT a users');
    let upd_user = usersFile[request.params.id - 1];
    let status;
    let mensaje;
    if (upd_user == undefined) {
      mensaje = {"msg":"Usuario no existe"};
      status = 404;
    } else {
      upd_user.first_name = request.body.first_name;
      upd_user.last_name = request.body.last_name;
      upd_user.email = request.body.email;
      upd_user.password = request.body.password;
      console.log(upd_user);
      mensaje = {"msg":"Usuario modificado correctamente"};
      status = 200;
    }
    response.status(status);
    response.send(mensaje);
});

//Petición DELETE a users
app.delete(URL_BASE + 'users/:id',
    function(request, response){
    console.log('DELETE a users');
    usersFile.splice(request.params.id - 1, 1);
    response.status(200);
    response.send({"msg":"Usuario eliminado correctamente " + request.params.id});
});

// LOGIN - user.json
app.post(URL_BASE + 'login',
  function(request, response) {
    console.log("POST /apicol/v2/login");
    console.log(request.body.email);
    console.log(request.body.password);
    var user = request.body.email;
    var pass = request.body.password;
    for(us of usersFile) {
      if(us.email == user) {
        if(us.password == pass) {
          us.logged = true;
          writeUserDataToFile(usersFile);
          console.log("Login correcto!");
          response.status(201).send({"msg" : "Login correcto.", "idUsuario" : us.id_user, "logged" : "true"});
        } else {
          console.log("Login incorrecto.");
          response.status(404).send({"msg" : "Login incorrecto."});
        }
      }
    }
    if(us.logged != true){
      console.log("Login incorrecto.");
      response.status(404).send({"msg" : "Login incorrecto por email."});
    }
});

// LOGOUT - user.json
app.post(URL_BASE + 'logout',
  function(request, response) {
    console.log("POST /apicol/v2/logout");
    var userId = request.body.id;
    for(us of usersFile) {
      if(us.id_user == userId) {
        if(us.logged) {
          delete us.logged; // borramos propiedad 'logged'
          writeUserDataToFile(usersFile);
          console.log("Logout correcto!");
          response.status(201).send({"msg" : "Logout correcto.", "idUsuario" : us.id_user});
        } else {
          console.log("Logout incorrecto.");
          response.status(404).send({"msg" : "Logout incorrecto."});
        }
      }
    }
    console.log("Logout incorrecto.");
    response.status(404).send({"msg" : "Logout incorrecto por user."});
});

function writeUserDataToFile(data) {
  var fs = require('fs');
  var jsonUserData = JSON.stringify(data);
  fs.writeFile("./user.json", jsonUserData, "utf8",
   function(err) { //función manejadora para gestionar errores de escritura
     if(err) {
       console.log(err);
     } else {
       console.log("Datos escritos en 'users.json'.");
     }
   });
};
